from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import authentication

from .models import CustomUser, Post, Comment
from .permissions import IsOwnerOrReadOnly

from .serializers import PostSerializer, CommentSerializer

class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    authentication_classes = ()
    permission_classes = ()
    
class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    authentication_classes = ()
    permission_classes = (IsOwnerOrReadOnly,)